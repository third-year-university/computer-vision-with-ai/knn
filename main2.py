import numpy as np
import matplotlib.pyplot as plt
import cv2
import pathlib
from tqdm import tqdm
from skimage.measure import regionprops, label

text_images = [plt.imread(path) for path in pathlib.Path("images").glob("*.png")]

print(len(text_images))

train_images = {}

for path in tqdm(sorted(pathlib.Path("images/train").glob("*"))):
    symbol = path.name[-1]
    train_images[symbol] = []
    for image_path in sorted(path.glob("*.png")):
        train_images[symbol].append(plt.imread(image_path))


def extract_features(image):
    if image.ndim == 3:
        gray = np.mean(image, 2)
        gray[gray > 0] = 1
        labeled = label(gray)
    else:
        labeled = image.astype("uint8")
    props = regionprops(labeled)[0]
    extent = props.extent
    eccentricity = props.eccentricity
    euler = props.euler_number
    rr, cc = props.centroid_local
    rr /= props.image.shape[0]
    cc /= props.image.shape[1]
    feret = (props.feret_diameter_max - 1) / np.max(props.image.shape)
    return np.array([extent, eccentricity, euler, rr, cc, feret], dtype="f4")


knn = cv2.ml.KNearest_create()

train = []
responses = []

sym2class = {symbol: i for i, symbol in enumerate(train_images)}
class2sym = {value: key for key, value in sym2class.items()}

for i, symbol in tqdm(enumerate(train_images)):
    for image in train_images[symbol]:
        train.append(extract_features(image))
        responses.append(sym2class[symbol])

train = np.array(train, dtype="f4")
responses = np.array(responses).reshape(-1, 1)

knn.train(train, cv2.ml.ROW_SAMPLE, responses)

features = extract_features(train_images["A"][0]).reshape(1, -1)

ret, results, neighbours, dist = knn.findNearest(features, 5)
print(ret, results, neighbours, dist)
print(class2sym[int(results[0][0])])


def image2text(image) -> str:
    gray = np.mean(image, 2)
    gray[gray > 0] = 1
    labeled = label(gray)
    regions = regionprops(labeled)

    regions.sort(key=lambda x: x.bbox[1])

    spaces = []
    for i in range(1, len(regions)):
        spaces.append(regions[i].bbox[1] - regions[i - 1].bbox[3])

    average = np.average(spaces)
    standard = np.std(spaces)

    answer = []

    prev_region = 0

    direction = "down"

    if regions[0].bbox[0] < regions[len(regions) - 1].bbox[0]:
        direction = "up"

    for region in regions:

        if region.bbox[1] - prev_region > (average + standard):
            answer.append(' ')
        elif region.bbox[1] - prev_region < (average - standard):
            if direction == "down":
                continue
            elif len(answer) != 0:
                answer.pop()

        prev_region = region.bbox[3]

        features = extract_features(region.image).reshape(1, -1)
        ret, results, neighbours, dist = knn.findNearest(features, 5)
        answer.append(class2sym[int(ret)])
    return "".join(answer)


for image in text_images:
    print(image2text(image))
