import numpy as np
import matplotlib.pyplot as plt
import cv2

n = 2000

np.random.seed(12)

xk1 = 100 + np.random.randint(-25, 25, n)
yk1 = 100 + np.random.randint(-25, 25, n)
rk1 = np.repeat(1, n)

xk2 = 150 + np.random.randint(-25, 25, n)
yk2 = 150 + np.random.randint(-25, 25, n)
rk2 = np.repeat(2, n)

new_point = (124, 127) # 1


knn = cv2.ml.KNearest_create()

train = np.stack([np.hstack([xk1, xk2]), np.hstack([yk1, yk2])])

train = train.T.astype('f4')

responses = np.hstack([rk1, rk2]).reshape(-1, 1).astype('f4')

# print(train.shape)
# print(responses.shape)

knn.train(train, cv2.ml.ROW_SAMPLE, responses)

in_ = np.array(new_point).astype("f4").reshape(1, 2)
ret, results, neighbours, dist = knn.findNearest(in_, 10)

plt.scatter(xk1, yk1)
plt.scatter(xk2, yk2, color='orange')
plt.show()

print(ret)
print(results)
print(neighbours)
print(dist)